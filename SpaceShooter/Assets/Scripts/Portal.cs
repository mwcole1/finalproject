﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
	private Background_helper gameController;
	//public GameObject pop;
	void Start(){
		GameObject gameControllerObject = GameObject.FindWithTag ("BackGround");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<Background_helper>();
		}
		if (gameControllerObject == null) {
			Debug.Log ("Can't find GameController");
		}

	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "boundary") {
			return;
		}
		//Instantiate(pop, transform.position, transform.rotation);
		if (other.tag == "Player") {
			gameController.BackgroundPick ();
		}
		Destroy (gameObject);
	}
	// Update is called once per frame
	void Update () {
		
	}
}
