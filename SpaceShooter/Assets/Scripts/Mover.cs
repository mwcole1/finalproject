﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour 
{
	public float speed;
	void Start(){
		int speed_multi = Random.Range (0, 7);
		int speed_multi2 = Random.Range (0, 5);
		Rigidbody rb = GetComponent<Rigidbody> ();
		if (speed_multi == 5) {
			
			rb.velocity = transform.forward * speed * 2;
		} 
		if (speed_multi2 == 4) {
			rb.velocity = transform.forward * speed * 1.5f;
		}
		else {
			rb.velocity = transform.forward * speed;
		}
	}

}
