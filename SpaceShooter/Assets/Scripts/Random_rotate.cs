﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Random_rotate : MonoBehaviour {

	public float tumbler;
	private Rigidbody rb; 
	void Start () {
		rb = GetComponent<Rigidbody> ();
		rb.angularVelocity = Random.insideUnitSphere * tumbler;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
