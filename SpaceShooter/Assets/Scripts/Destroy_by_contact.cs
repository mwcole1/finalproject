﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_by_contact : MonoBehaviour {
	public GameObject explosin;
	public GameObject playerexplosin;
	public int ScoreValue;
	private GameController gameController;
	private bool protect;
	//bool Protect=false;
	void Start(){
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController> ();
		}
		if (gameControllerObject == null) {
			Debug.Log ("Can't find GameController");
		}

	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "boundary") {
			return;
		}
		Instantiate (explosin, transform.position, transform.rotation);
		protect = gameController.Protection ();
		if (other.tag == "Player") {
			if (protect) {
				Destroy (gameObject);
				gameController.addScore (ScoreValue);
				return;
			} 
			else {
				gameController.Lives_remove ();
				if (!gameController.Alive ()) {
					Instantiate (playerexplosin, other.transform.position, other.transform.rotation);
					Destroy (gameObject);
					gameController.addScore (ScoreValue);
					gameController.GameOver ();
					Destroy (other.gameObject);
				} else {
					Instantiate (explosin, other.transform.position, other.transform.rotation);
					Destroy (gameObject);
					gameController.addScore (ScoreValue);
				}

			}
        }
        else
		if (other.tag == "Beam") {
			Instantiate (explosin, gameObject.transform.position, gameObject.transform.rotation);
			Destroy (gameObject);
			gameController.addScore (ScoreValue);
		}else {
			Instantiate (explosin, gameObject.transform.position, gameObject.transform.rotation);
			Destroy (gameObject);
			gameController.addScore (ScoreValue);
			Destroy (other.gameObject);
		}
	}

}
