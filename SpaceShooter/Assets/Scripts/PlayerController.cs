﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary{
	public float xMin;
	public float xMax;
	public float zMin;
	public float zMax;
}

public class PlayerController : MonoBehaviour {
	public GameObject player;
	private Rigidbody rb;
	public float speed;
	public float tilt;
	public Boundary boundary;
	public GameObject shot;
	public GameObject beam;
	public Transform shotSpawn;
	public Transform shotSpawn2;
	public Transform shotSpawn3;
	private float nextFire = 0.5f;
	public float fireRate;
	private AudioSource audioSource;
	public int Rounds;
	private GameController gameController;
	public ParticleSystem protect;
	public ParticleSystem multi;
	public bool particleProtOn;
	public bool multiON;
	public Color original;
	private Color bubblecolor;
	public bool BeamOn=true;
	private bool isorig=true;
	void Start(){
		
		audioSource = GetComponent<AudioSource> ();
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController> ();
		}
		if (gameControllerObject == null) {
			Debug.Log ("Can't find GameController");
		}
		Rounds = gameController.Current_rounds ();
		particleProtOn = false;
		multiON = false;
	}
	void Update()
	{if (gameController.ColorOn()&& isorig) {
			bubblecolor = gameController.Changeto();
			ColorChange ();
		}
		if (gameController.Protection()&&!particleProtOn) {
			particleProtect ();
		}
		if (gameController.MultiOn()&&!multiON) {
			particleMulti ();
		}
		Rounds = gameController.Current_rounds ();
		if (Input.GetButton("Fire1")&& Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
			//GameObject clone=
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			audioSource.Play ();
			//as GameObject;
		}
		if((Input.GetKeyDown(KeyCode.Space) && Time.time>nextFire)&& Rounds>0){
			nextFire=Time.time+fireRate;
			Instantiate(shot,shotSpawn.position,shotSpawn.rotation);
			Instantiate(shot,shotSpawn2.position,shotSpawn2.rotation);
			Instantiate(shot,shotSpawn3.position,shotSpawn3.rotation);
			audioSource.Play();
			//print (Rounds);
			gameController.Round_off ();
		}
		if((Input.GetKeyDown(KeyCode.F) && Time.time>nextFire)&& gameController.BeamCharge()){
			nextFire=Time.time+fireRate;
			Instantiate(beam,shotSpawn.position,shotSpawn.rotation);
			audioSource.Play();
			//print (Rounds);
			gameController.BeamUncharge ();
		}
	}
	void FixedUpdate(){
		float movementhorizontal = Input.GetAxis ("Horizontal");
		float movementvertical = Input.GetAxis ("Vertical");
		rb = GetComponent<Rigidbody> ();
		Vector3 movement=new Vector3 (movementhorizontal, 0.0f, movementvertical);
		rb.velocity = movement*speed;
		rb.position = new Vector3 (Mathf.Clamp(rb.position.x,boundary.xMin,boundary.xMax), 0.0f, Mathf.Clamp(rb.position.z,boundary.zMin,boundary.zMax));
		rb.rotation = Quaternion.Euler (0.0f,0.0f,(rb.velocity.x * -tilt));
	}
	public void particleProtect(){
		particleProtOn = true;
		protect.Play ();
		Invoke ("particleOffProtect", 5);
	}
	public void particleOffProtect(){
		protect.Stop ();
		particleProtOn = false;
	}
	public void particleMulti(){
		multi.Play ();
		multiON = true;
		Invoke ("particleOffMulti", 10);
	}
	public void particleOffMulti(){
		multi.Stop ();
		multiON = false;
	}
	public void ColorChange(){
		gameObject.GetComponent<Renderer> ().material.color = bubblecolor;
		isorig = false;
		Invoke ("ChangeOrg", 5);
	}
	public void ChangeOrg(){
		gameObject.GetComponent<Renderer> ().material.color = original;
		isorig = true;
		print ("called");
	}
	public void BeamRecharge(){
		BeamOn = true;
	}
}
