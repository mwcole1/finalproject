﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble_Pop : MonoBehaviour {
    public enum BubbleColor {red,blue,green };
    public BubbleColor mycolor;
	public GameObject pop;
	public Color material;
	private GameController gameController;
	private bool protect;
	//bool Protect=false;
	void Start(){
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController> ();
		}
		if (gameControllerObject == null) {
			Debug.Log ("Can't find GameController");
		}

	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "boundary") {
			return;
		}
		Instantiate(pop, transform.position, transform.rotation);
		if (other.tag == "Player") {
			if (mycolor == BubbleColor.blue) {
				gameController.blue_on ();
			}
			if (mycolor == BubbleColor.green) {
				gameController.green_on ();
			}
            if (mycolor==BubbleColor.red) {
				gameController.red_on ();
			}
			//print ("uh oh");
			//Instantiate (playerexplosin, other.transform.position, other.transform.rotation);
			//gameController.GameOver ();
		}
		//Destroy (other.gameObject);
		Destroy (gameObject);
		//print ("it gone");
		//gameController.addScore (ScoreValue);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
