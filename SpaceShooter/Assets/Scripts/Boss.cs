﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour {
	public int mycolor_num;
	public GameObject explosin;
	public int Scorevalue;
	public Color shipc;
	private GameController gameController;
	int hits = 5;
	public GameObject nodamage;
	void Start(){
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController> ();
		}
		if (gameControllerObject == null) {
			Debug.Log ("Can't find GameController");
		}
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "boundary") {
			return;
		}
		Instantiate (explosin, transform.position, transform.rotation);
		if (other.tag == "Player") {
			gameController.Lives_remove ();
			hits = hits - 1;
			if (!gameController.Alive ()) {
				print ("Beam2");
				Instantiate (explosin, other.transform.position, other.transform.rotation);
				Destroy (gameObject);
				gameController.addScore (10);
				gameController.GameOver ();
				Destroy (other.gameObject);
			} else {
				Instantiate (explosin, other.transform.position, other.transform.rotation);
				print ("beam1");
				if (hits > 0) {
					gameController.addScore (10);
				} else {
					gameController.addScore (100);
					gameController.bosskilled ();
					Destroy (gameObject);
				}
			}
				
		}
		else {
			int PlayerColor = gameController.CurrentColor ();
			if (mycolor_num == PlayerColor) {
				print ("shouldnt be here");
				Instantiate (explosin, gameObject.transform.position, gameObject.transform.rotation);
				hits = hits - 1;
				Destroy (other.gameObject);
				gameController.addScore (10);
				if (hits <= 0) {
					Destroy (gameObject);
					gameController.bosskilled ();
					gameController.addScore (100);
				}
			} else {
				Instantiate (nodamage, gameObject.transform.position, gameObject.transform.rotation);
				Destroy (other.gameObject);
				print ("created spot");
			}
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
}
