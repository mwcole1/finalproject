﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	public int player_num=0;
	public GameObject portal;
	private GameObject hazard;
	public GameObject normal_hazard;
	public GameObject Fire_hazard;
	public GameObject Ice_hazard;
	public Vector3 spawnValues;
	public int hazardCount;
	public float SpawnWait;
	public float StartWait;
	public float WaveLength;
	public GUIText ScoreText;
	public GUIText RestartText;
	public GUIText EndGameText;
	public GUIText LivesText;
	private bool gameover;
	private bool restart;
	private int Score;
	private bool multi;
	private bool protect;
	public GameObject Can;
	public GameObject Power;
	private int Rounds;
	private int Lives;
	private GameController gameController;
	public int wave;
	private float extra;
	private int portalChance;
	private int lastScore;
	private bool colorChange;
	private bool bossdead=true;
	private bool bossspawn=false;
	public GameObject Boss1;
	public GameObject Boss2;
	public GameObject Boss3;
	public GameObject color1;
	public GameObject color2;
	public GameObject color3;
	Color lastbubble;
	Color currentbubble;
	private bool BeamOn=true;
	void Start () {
		hazard = normal_hazard;
		colorChange = false;
		Score = 0;
		lastScore = 0;
		Lives = 3;
		multi = false;
		protect = false;
		UpdateScore ();
		UpdateLives ();
		StartCoroutine (SpawnWaves ());
		gameover = false;
		restart = false;
		RestartText.text = "";
		EndGameText.text = "";
		Rounds = 5;
		wave = 2;

	}
	void Update(){
		if (Score % 2000==0 && Score!=lastScore) {
			Lives_add ();
		}
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				Application.LoadLevel (Application.loadedLevel);
			}
		}
	}
	IEnumerator SpawnWaves(){
		while (true) {
			if (wave % 10 == 0 && wave != 0 && bossdead) {
				//print ("bossspawn");
				//print (wave);
				Vector3 spawnPosition = Boss1.transform.position;
                Quaternion spawnRotation = Boss1.transform.rotation;
				int bosspick = Random.Range (0, 2);
				if (bosspick == 0) {
					Instantiate (Boss1, spawnPosition, spawnRotation);
					bossdead = false;
				}
				if (bosspick == 1) {
					Instantiate (Boss2, spawnPosition, spawnRotation);
					bossdead = false;
				}
				if (bosspick == 2) {
					Instantiate (Boss3, spawnPosition, spawnRotation);
					bossdead = false;
				}
			}
			if(!bossdead){
					Vector3 spawnPosition2 = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
					int randomSphere = Random.Range (0, 2);
					Quaternion spawnRotation = Quaternion.identity;
					if (randomSphere == 0) {
						Instantiate (color1, spawnPosition2, spawnRotation);
					}
					if (randomSphere == 1) {
						Instantiate (color2, spawnPosition2, spawnRotation);
					}
					if (randomSphere == 2) {
						Instantiate (color3, spawnPosition2, spawnRotation);
					}
					
			} else {

				for (int i = 0; i < hazardCount * wave; i++) {
					Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
					Quaternion spawnRotation = Quaternion.identity;
					extra = Random.Range (0, 50);
					portalChance = Random.Range (0, 30);
					if (portalChance == 1) {
						//print ("Spawned");
						Instantiate (portal, portal.transform.position, portal.transform.rotation);
					} else if (extra == 0 || extra == 25) {
						if (i == 0) {
							Instantiate (Power, spawnPosition, spawnRotation);
						} else {
							Instantiate (Can, spawnPosition, spawnRotation);
						}
					} else {
						Instantiate (hazard, spawnPosition, spawnRotation);
					}
					yield return new WaitForSeconds (SpawnWait);
				
				}
				wave += 1;
			}

		
			yield return new WaitForSeconds (WaveLength);
			if (gameover) {
				RestartText.text = "Press 'R' to Restart";
				restart = true;
				break;
			}
		}
	}
	public void addScore(int newScoreValue){
		if (multi) {
			Score += newScoreValue * 5;
			UpdateScore ();
		} else {
			Score += newScoreValue;
			UpdateScore ();
		}
	}
	public bool MultiOn(){
		return multi;
	}
	public void Multiplier(){
		multi = true;
		Invoke ("Multiplier_off", 10);
	}
	public void Multiplier_off(){
		multi = false;

	}

	// Update is called once per frame
	void UpdateScore () {
		ScoreText.text = "Score " + Score;
	}
	void UpdateLives(){
		LivesText.text = "Lives " + Lives;
	}
	public void GameOver(){
		EndGameText.text = "Game Over";
		gameover = true;
	}
	public bool Alive(){
		if (Lives <= 0) {
			return false;
		} else {
			return true;
		}
	}
	public bool Protection(){
		return protect;
	}
	public void Protection_on(){
		protect = true;
		Invoke ("Protection_off", 5);
	}
	public void Protection_off(){
		protect = false;
	}
	public int Current_rounds(){
		return Rounds;
	}
	public void Round_add(){
		if (Rounds < 5) {
			Rounds += 1;
		}
	}
	public void Round_off(){
		if (Rounds > 0) {
			Rounds -= 1;
			Invoke ("Round_add", 15);
		}
	}
	public void Lives_add(){
		lastScore = Score;
		Lives += 1;
		UpdateLives ();
	}
	public void Lives_remove(){
		Lives -= 1;
		UpdateLives ();
	}
	public void blue_on(){
        print("blue");
		colorChange = true;
		lastbubble = Color.blue;
		player_num = 1;
        Invoke("colorOff", 5);
    }
	public void red_on(){
        print("red");
		colorChange = true;
		lastbubble = Color.red;
		player_num = 3;
		Invoke ("colorOff", 5);
	}
	public void green_on(){
        print("green");
		colorChange = true;
		lastbubble = Color.green;
		player_num = 2;
		Invoke ("colorOff", 5);
	}
	public void colorOff(){
		colorChange = false;
		player_num = 0;
		lastbubble = Color.white;
		Invoke ("colorOff", 5);
	}
	public bool ColorOn(){
		if (colorChange) {
			return true;
		}
		return false;
	}
	public Color Changeto(){
		return lastbubble;
	}
	public int CurrentColor(){
		return player_num;
	}
	public void IceChange(){
		hazard = Ice_hazard;
	}
	public void FireChange(){
		hazard = Fire_hazard;
	}
	public void normChange(){
		hazard = normal_hazard;
	}
	public void BeamRecharge(){
		BeamOn = true;
	}
	public void BeamUncharge(){
		BeamOn = false;
		Invoke ("BeamRecharge", 30);
	}
	public bool BeamCharge(){
		return BeamOn;
	}
	public void bosskilled(){
		bossdead=true;
		wave += 1;
	}
}
