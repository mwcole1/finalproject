﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverforDoor : MonoBehaviour {

	public float speed;
	void Start(){
		int speed_multi = Random.Range (0, 10);
		Rigidbody rb = GetComponent<Rigidbody> ();
		if (speed_multi == 5) {

			rb.velocity = -transform.right * speed * 2;
		} else {
			rb.velocity = -transform.right * speed;
		}
	}
}
