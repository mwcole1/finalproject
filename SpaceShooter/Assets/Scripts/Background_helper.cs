﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background_helper : MonoBehaviour {
	public Material ice;
	public Material fire;
	public Material orig;
	private Material background;
	private GameController gameController;
	void Start () {
		background = orig;
		gameObject.GetComponent<Renderer> ().material = background;
	
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController> ();
		}
		if (gameControllerObject == null) {
			Debug.Log ("Can't find GameController");
		}


	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void BackgroundPick(){
		int num = Random.Range (0, 2);
		if (num == 0) {
			background = orig;
			gameObject.GetComponent<Renderer> ().material = background;
			gameController.normChange ();
		}
		if (num == 1) {
			background = ice;
			gameObject.GetComponent<Renderer> ().material = background;
			gameController.IceChange ();
		} else {
			background = fire;
			gameObject.GetComponent<Renderer> ().material = background;
			gameController.FireChange ();
		}
	}
}
